const {
  insert,
  search,
  getById,
  update,
  deleteById,
} = require("./students.service");

const setUpRoutes = (app) => {
  console.log("setup routes initialized");
  app.post("/api/students/create", async (req, res) => {
    const result = await insert(req.body);
    console.log(req.body);
    res.send(result);
  });

  app.post("/api/students/search", async (req, res) => {
    const result = await search(req.body);
    console.log(req.body);
    res.send(result);
  });

  app.get("/api/students/details/:id", async (req, res) => {
    const result = await getById(req.params.id);
    console.log(req.params.id);
    res.send(result);
  });

  app.put("/api/students/update/:id", async (req, res) => {
    const result = await update(req.params.id, req.body);
    console.log(req.params.id, req.body);
    res.send(result);
  });

  app.delete("/api/students/delete/:id", async (req, res) => {
    const result = await deleteById(req.params.id);
    console.log(req.params.id);
    res.send(result);
  });
};

module.exports = {
  setUpRoutes,
};
