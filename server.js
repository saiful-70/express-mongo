const app = require("./app");
const { connect } = require("./mongo");

const PORT = 3000;

const setup = () => {
  const { setUpRoutes } = require("./students.controller");
  setUpRoutes(app);
};

app.listen(PORT, async () => {
  console.log(`Listening to port ${PORT}`);
  await connect();
  setup();
  app.use("/", (req, res) => {
    console.log("request recieved");
    res.send(`request recieved at ${new Date()}`);
  });

  console.log("connected to mongodb");
});
