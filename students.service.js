const { ObjectId } = require("mongodb");
const {Student} = require("./student.model")

const insert = async (document) => {
  const result = await Student.insertOne(document);
  return result;
};

const search = async (document) => {
  const result = await Student.find(document).toArray();
  return result;
};

const getById = async (id) => {
  const result = await Student.findOne({ _id: new ObjectId(id) });
  return result;
};

const update = async (id, document) => {
  const result = await Student.updateOne(
    { _id: new ObjectId(id) },
    { $set: document }
  );
  return result;
};
const deleteById = async (id) => {
  const result = await Student.deleteOne({ _id: new ObjectId(id) });
  return result;
};

module.exports = {
  insert,
  search,
  getById,
  update,
  deleteById,
};
