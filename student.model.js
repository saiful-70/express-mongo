const { getDb } = require("./mongo");

const getCollection = () => {
  console.log(`creating student collection`);
  const db = getDb();
  const collection = db.collection("students", {
    validator: {
      $jsonSchema: {
        bsonType: "object",
        required: ["name", "phone", "age", "city"],
        properties: {
          name: {
            bsonType: "string",
            description: "must be a string and is required",
          },
          phone: {
            bsonType: "string",
            description: "must be a string and is required",
          },
          age: {
            bsonType: "int",
            minimum: 0,
            maximum: 50,
            description: "must be an integer in [ 0, 50 ] and is required",
          },
          city: {
            enum: ["Dhaka", "Chittagong", "Rajshahi", "Khulna", "Sylhet"],
            description: "can only be one of the enum values and is required",
          },
        },
      },
    },
  });
  return collection;
};

module.exports = {
  Student: getCollection(),
};